import mlflow
from dotenv import load_dotenv

load_dotenv()


class Model:
    def __init__(self, model_name: str, model_stage: str) -> None:
        model_path = f"models:/{model_name}/{model_stage}"
        print(f"model_path: {model_path}")

        self.model = mlflow.prophet.load_model(model_path)
        print(f"model: {self.model}")

        pass

    def predict(self, data):
        predictions = self.model.predict(data)
        return predictions
