from fastapi import FastAPI, File, UploadFile, HTTPException
import pandas as pd
import os
from .model import Model

app = FastAPI(debug=True)
model = Model(model_name="forecasting_model", model_stage="Staging")


@app.get("/")
async def root():
    return {"message": "Model inference"}


# POST endpoint woth path /invocations
@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    print(f"file: {file.filename}")

    if file.filename.endswith(".csv"):
        with open(file.filename, "wb") as f:
            f.write(file.file.read())

        data = pd.read_csv(file.filename)
        print(data.head())

        os.remove(file.filename)
        prediction_results = model.predict(data)
        print(prediction_results.head())

        return list(prediction_results)

    else:
        raise HTTPException(
            status_code=400, detail="Invalid file format. Only .csv files accepted."
        )
